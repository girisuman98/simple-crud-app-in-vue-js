<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/authenticate', 'UserController@login');
Route::get('/validateToken', function () {
    return ['data' => 'Token is valid'];
})->middleware('auth:api');
Route::group(['middleware' => 'auth:api'], function(){

	Route::get("/products", "ProductController@index");
	Route::post("/product/store", "ProductController@store");
	Route::get("/product/details/{id}", "ProductController@details");
	Route::post("/product/update/{id}", "ProductController@update");
	Route::delete('/product/delete/{id}', 'ProductController@delete');
	Route::post("/logout", "ProductController@logout");
});