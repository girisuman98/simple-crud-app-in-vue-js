/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';

import App from './App.vue';
Vue.use(VueAxios, axios);

import Vuelidate from 'vuelidate';
Vue.use(Vuelidate);

import { validationMixin } from 'vuelidate';

import Product from './components/product/ProductComponent.vue'
import CreateProduct from './components/product/CreateProductComponent.vue'
import UpdateProduct from './components/product/UpdateProductComponent.vue'
import Index from './components/IndexComponent.vue'
import Dashboard from './components/DashboardComponent.vue'
import Login from './components/LoginComponent.vue'

const routes = [
  	{
		name: 'index',
        path:'/',
        component:Index
    },
	{
		name: 'login',
        path:'/login',
        component:Login
    },
	{
		name: 'dashboard',
        path:'/dashboard',
        component:Dashboard
    },
    { 
		name: 'products',
        path:'/products',
        component:Product
    },
	{ 
		name: 'productCreate',
        path:'/product/create',
        component:CreateProduct
    },
	{ 
		name: 'productUpdate',
        path:'/product/edit/:id',
        component:UpdateProduct
    },
];

const router = new VueRouter({ mode: 'history', routes: routes});
const app = new Vue(Vue.util.extend({ router }, App)).$mount('#app');