<?php

namespace App\Http\Controllers;

use Hash;
use Validator;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * LOGIN.
     *
     */
    public function login (Request $request) {
		$validator = Validator::make($request->all(), [
			'email' => 'required|string|email|max:255',
			'password' => 'required|string|min:6',
		]);
		if ($validator->fails())
		{
			return response(['errors'=>$validator->errors()->all(), "status" => false], 200);
		}
		$user = User::where('email', $request->email)->first();
		if ($user) {
			if (Hash::check($request->password, $user->password)) {
				$token = $user->createToken('Laravel Password')->accessToken;
				$response = ['user' => $user, "status" => true];
				return response($response, 200)->header('Authorization', 'Bearer '.$token);
			} else {
				$response = ["message" => "Password mismatch", "status" => false];
				return response($response, 200);
			}
		} else {
			$response = ["message" =>'User does not exist', "status" => false];
			return response($response, 200);
		}
	}

    /**
     * LOGOUT
     *
     */
    public function logout (Request $request) {
		$token = $request->user()->token();
		$token->revoke();
		$response = ['message' => 'You have been successfully logged out!'];
		return response($response, 200);
	}
}
