<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Validator;
use App\User;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
	{
		$data = Product::all();
		$response = ['data' => $data];
		return response($response, 200);
    }
	
	public function store(Request $request){
		
		$validator = Validator::make($request->all(), [
			"name"    => "required|min:3",
			"price"    => "required"
		]);
		
		if($validator->fails()){
            return response(['errors'=>$validator->errors()->all()], 200);
        }
		
		DB::beginTransaction();
		try {
		
			$product = new Product();
			
			$product->name = $request->name;
			$product->price = $request->price;
			$product->user_id = Auth::user()->id;
			$product->save();
			
			DB::commit();	
			
			$response = ["message" => "Product created successfully"];
			return response($response, 200);
			
		} catch (\Exception $e) {
			DB::rollback();
			DB::commit();
			throw new \Exception('Exception: '. $e);
		}
	}
	
	public function details($id)
	{
		$data = Product::find($id);
        $response = ['data' => $data];
		return response($response, 200);
    }
	
	public function update(Request $request, $id){
		
		$validator = Validator::make($request->all(), [
			"name"    => "required|min:3",
			"price"    => "required"
		]);
		
		if($validator->fails()){
            return response(['errors'=>$validator->errors()->all()], 200);
        }
		
		DB::beginTransaction();
		try {
		
			$product = Product::find($id);
			
			if(!Gate::allows('update-product', $product)) {
			  	$response = ["message" => "You are not allowed to update this product"];
				return response($response, 200);
			}
			
			$product->name = $request->name;
			$product->price = $request->price;
			$product->save();
			
			DB::commit();	
			$response = ["message" => "Product updated successfully"];
			return response($response, 200);
			
		} catch (\Exception $e) {
			DB::rollback();
			DB::commit();
			throw new \Exception('Exception: '. $e);
		}
	}
	
	public function delete($id)
    {
		$user = Auth::user();
		$product = Product::find($id); 
		if (!$user->can('update', $product)) {
			$response = ["message" => "You are not allowed to delete the product", "status" => false];
		  	return response($response, 200);
		}
		if($product == NULL){
			$response = ["message" => "Product does not exist", "status" => false];
			return response($response, 200);	
		}
		$product->delete();
		$response = ["message" => "Product deleted successfully", "status" => true];
		return response($response, 200);	
	}
	
	public function logout(Request $request)
	{        
	    $token = Auth::user()->token();
	    $token->revoke();
	    $response = ["message" => "Logged out successfully", "status" => true];
		return response($response, 200);	
	}
	
}
